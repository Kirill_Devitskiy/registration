'use strict'

// Вход
let authorizationEntry = document.querySelector('#authorizationEntry');
let formEntry = document.querySelector('#formEntry');
let subEmailEntry = document.querySelector('#subEmailEntry');
let starEmailEntry = document.querySelector('#starEmailEntry');
let emailEntry = document.querySelector('#emailEntry');
let errorEmailEntry = document.querySelector('#errorEmailEntry');
let errrorEmailValidEntry = document.querySelector('#errrorEmailValidEntry');
let subPasswordEntry = document.querySelector('#subPasswordEntry');
let starPasswordEntry = document.querySelector('#starPasswordEntry');
let passwordEntry = document.querySelector('#passwordEntry');
let errorPasswordEntry = document.querySelector('#errorPasswordEntry');
let errrorPassLengthEntry = document.querySelector('#errrorPassLengthEntry');
let starLabelEntry = document.querySelector('#starLabelEntry');
let checkmarkEntry = document.querySelector('#checkmarkEntry');
let errorLabelEntry = document.querySelector('#errorLabelEntry');
let buttonEntry = document.querySelector('#buttonEntry');
let userErrorEntry = document.querySelector('#userErrorEntry')

let authorizationEntryData = '';
let emailEntryData = '';
let passwordEntryData = '';
let checkmarkEntryData = false;

authorizationEntry.addEventListener ('click', (e) => {
  e.preventDefault()

  if (authorizationEntryData == false) {
    formEntry.style.display = 'none';
    formReg.style.display = 'block';
  }
})

emailEntry.addEventListener('input', (e) => {
  emailEntryData = e.target.value;
})

passwordEntry.addEventListener('input', (e) => {
  passwordEntryData = e.target.value;
})

checkmarkEntry.addEventListener('click', (e) => {
  checkmarkEntryData = e.checked;
})

function validateEmail(emailEntryData) {
  const re =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    return re.test(String(emailEntryData).toLowerCase());
}

buttonEntry.addEventListener('click', (e) => {
  e.preventDefault()

if (emailEntryData.length == 0) {
  errorEmailEntry.style.display = 'block';
  errrorEmailValidEntry.style.display = 'none';
  emailEntry.style.borderColor = 'red';
  starEmailEntry.style.color = 'red';
  subEmailEntry.style.color = 'red';
} else {
  errorEmailEntry.style.display = 'none';
  emailEntry.style.borderColor = '#787878';
  starEmailEntry.style.color = '#787878';
  subEmailEntry.style.color = '#787878';
} 

if (validateEmail(emailEntryData) == false && emailEntryData.length > 0) {
  errrorEmailValidEntry.style.display = 'block';
  emailEntry.style.borderColor = 'red';
  starEmailEntry.style.color = 'red';
  subEmailEntry.style.color = 'red';
} else if (validateEmail(emailEntryData) != false && emailEntryData.length > 0) {
  errrorEmailValidEntry.style.display = 'none';
  emailEntry.style.borderColor = '#787878';
  starEmailEntry.style.color = '#787878';
  subEmailEntry.style.color = '#787878';
}

if (passwordEntryData.length == 0) {
  errorPasswordEntry.style.display = 'block';
  errrorPassLengthEntry.style.display = 'none';
  passwordEntry.style.borderColor = 'red';
  starPasswordEntry.style.color = 'red';
  subPasswordEntry.style.color = 'red';
} else if (passwordEntryData.length > 1 && passwordEntryData.length < 8) {
  errrorPassLengthEntry.style.display = 'block';
  errorPasswordEntry.style.display = 'none';
  passwordEntry.style.borderColor = 'red';
  starPasswordEntry.style.color = 'red';
  subPasswordEntry.style.color = 'red';
} else {
  errorPasswordEntry.style.display = 'none';
  errrorPassLengthEntry.style.display = 'none';
  passwordEntry.style.borderColor = '#787878';
  starPasswordEntry.style.color = '#787878';
  subPasswordEntry.style.color = '#787878';
}

if (checkmarkEntryData == false) {
  errorLabelEntry.style.display = 'block';
  checkmarkEntry.style.borderColor = 'red';
  starLabelEntry.style.color = 'red';
} else {
  errorLabelEntry.style.display = 'none';
  checkmarkEntry.style.borderColor = '#787878';
  starLabelEntry.style.color = '#787878';
}
})

// Регистрация
let authorizationReg = document.querySelector('#authorizationRegistration');
let formReg = document.querySelector('#formRegistration');
let subEmailReg = document.querySelector('#subEmailRegistration');
let starEmailReg = document.querySelector('#starEmailRegistration');
let emailReg = document.querySelector('#emailRegistration');
let errorEmailReg = document.querySelector('#errorEmailRegistration');
let errrorEmailValidReg = document.querySelector('#errrorEmailValidRegistration');
let subPasswordReg = document.querySelector('#subPasswordRegistration');
let starPasswordReg = document.querySelector('#starPasswordRegistration');
let passwordReg = document.querySelector ('#passwordRegistration');
let errorPasswordReg = document.querySelector('#errorPasswordRegistration');
let errrorPassLengthReg = document.querySelector('#errrorPassLengthRegistration');
let starLabelReg = document.querySelector('#starLabelRegistration');
let checkmarkReg = document.querySelector('#checkmarkRegistration');
let errorLabelReg = document.querySelector('#errorLabelRegistration');
let buttonReg = document.querySelector('#buttonRegistration');

let authorizationRegData = '';
let emailRegData = '';
let passwordRegData = '';
let checkmarkRegData = false;

authorizationReg.addEventListener ('click', (e) => {
  e.preventDefault()
  
  if (authorizationRegData == false) {
    formEntry.style.display = 'block';
    formReg.style.display = 'none';
  }
})

emailReg.addEventListener('input', (e) => {
  emailRegData = e.target.value;
})

passwordReg.addEventListener('input', (e) => {
  passwordRegData = e.target.value;
})

checkmarkReg.addEventListener('click', (e) => {
  checkmarkRegData = e.target.value;
})

function validateEmail(emailRegData) {
  const re =/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;    return re.test(String(emailRegData).toLowerCase());
}

buttonReg.addEventListener('click', (e) => {
  e.preventDefault()

  if (emailRegData.length == 0) {
    errorEmailReg.style.display = 'block';
    errrorEmailValidReg.style.display = 'none';
    emailReg.style.borderColor = 'red';
    starEmailReg.style.color = 'red';
    subEmailReg.style.color = 'red';
  }
  else {
    errorEmailReg.style.display = 'none';
    emailReg.style.borderColor = '#787878';
    starEmailReg.style.color = '#787878';
    subEmailReg.style.color = '#787878';
  }

  if (validateEmail(emailRegData) == false && emailRegData.length > 0) {
    errrorEmailValidReg.style.display = 'block';
    emailReg.style.borderColor = 'red';
    starEmailReg.style.color = 'red';
    subEmailReg.style.color = 'red';
  } else if (validateEmail(emailRegData) != false && emailRegData.length > 0) {
    errrorEmailValidReg.style.display = 'none';
    emailReg.style.borderColor = '#787878';
    starEmailReg.style.color = '#787878';
    subEmailReg.style.color = '#787878';
  }
  
  if (passwordRegData.length == 0) {
    errorPasswordReg.style.display = 'block';
    errrorPassLengthReg.style.display = 'none';
    passwordReg.style.borderColor = 'red';
    starPasswordReg.style.color = 'red';
    subPasswordReg.style.color = 'red';
  } else if (passwordRegData.length > 1 && passwordRegData.length < 8) {
    errrorPassLengthReg.style.display = 'block';
    errorPasswordReg.style.display = 'none';
    passwordReg.style.borderColor = 'red';
    starPasswordReg.style.color = 'red';
    subPasswordReg.style.color = 'red';
  } else {
    errorPasswordReg.style.display = 'none';
    errrorPassLengthReg.style.display = 'none';
    passwordReg.style.borderColor = '#787878';
    starPasswordReg.style.color = '#787878';
    subPasswordReg.style.color = '#787878';
  }
  
  if (checkmarkRegData == false) {
    errorLabelReg.style.display = 'block';
    checkmarkReg.style.borderColor = 'red';
    starLabelReg.style.color = 'red';
  } else {
    errorLabelReg.style.display = 'none';
    checkmarkReg.style.borderColor = '#787878';
    starLabelReg.style.color = '#787878';
  }
})

let regData = []

buttonReg.addEventListener('click', (e) => {
  e.preventDefault()

  if (emailRegData.length > 0 && validateEmail(emailRegData) != false && passwordRegData.length >= 8 && checkmarkRegData != false) {

    regData = [
      emailRegData, passwordRegData
    ];

    localStorage.setItem('regData', JSON.stringify(regData));
  }
})

let entryData = [];

buttonEntry.addEventListener('click', (e) => {

  entryData = [
    emailEntryData, passwordEntryData
  ]

  localStorage.setItem('entryData', JSON.stringify(entryData));

  if (localStorage.entryData == localStorage.regData && checkmarkEntryData != false) {

    document.location.href = 'https://ya.ru/?utm_referrer=https%3A%2F%2Fyandex.ru%2F';

  } else if (localStorage.entryData != localStorage.regData && checkmarkEntryData != false) {
    
    userErrorEntry.style.display = 'block';
  }
})